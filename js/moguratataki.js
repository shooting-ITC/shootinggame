enchant();
window.onload = function(){
    let game_ = new Game(320,320); //表示領域の大きさ設定
    game_.fps = 30; //ゲームの進行スピード
    game_.preload('./img/chara1.png','./img/start.png','./img/gameover.png'); //ゲームに使う素材をあらかじめ読み込む
    game_.onload = function(){ //ゲームの準備が整ったら、メインの処理を実行する。

        /*
        *タイトルシーン
        *
        * タイトルシーンを作り返す関数
        */

        let createStartScene = function(){
            let scene = new Scene(); //新しいシーンの作成
            scene.backgroundColor = '#fcc800'; //シーンの背景色

            //スタート画面
            //スタート画像設定へ
            let startImage = new Sprite(236,48); //スプライトの作成
            startImage.image = game_.assets['./img/start.png']; //スタート画像の設定
            startImage.x = 42; //横位置調整
            startImage.y = 136; //縦位置調整
            scene.addChild(startImage); //シーンに追加

            //タイトルラベル設定
            let title = new Label('クマたたき'); //ラベル文字
            title.textAlign = 'center'; //中央寄せ
            title.color = '#ffffff'; //文字色
            title.x = 0; //縦位置
            title.y = 96; //横位置
            title.font = '28px sans-serif'; //フォント(san-serifは、ゴシック体)
            scene.addChild(title); //シーンに追加

            //サブタイトルラベル設定
            let subTitle = new Label('- くまをたくさん叩こう -');
            subTitle.textAlign = 'center';
            subTitle.x = 0;
            subTitle.y = 196;
            subTitle.font = '14px sans-serif';
            scene.addChild(subTitle);

            //スタート画面にタッチイベントを設定
            startImage.addEventListener(Event.TOUCH_START,function(e){
                game_.replaceScene(createGameScene()); //現在表示しているスタートシーンをゲームシーンに置き換える。
                
            });
            return scene;
        };

        /*
        *ゲームシーン
        *
        *ゲームシーンを作り、返す関数
        */
       let createGameScene = function(){
           let scene = new Scene(); //新しいシーンの作成
           scene.backgroundColor = '#fcc8f0';
           let time = 240; //残り時間を初期化
           let score = 0; //得点を初期化

           //得点欄
           let label = new Label('スコア:'+score+'体叩いた！');
           label.font = '14px sans-serif';
           scene.addChild(label); //シーンに追加

           //制限時間欄
           let timeLimit = new Label('残り時間:'+time);
           timeLimit.font = '14px sans-serif';
           timeLimit.x = 0;
           timeLimit.y = 20;
           scene.addChild(timeLimit);

           //背景テキスト作成
           let backgroundText = new Label('くまをたたけ！');
           backgroundText.color = '#ffffff';
           backgroundText.font = '60px sans-serif';
           backgroundText.textAlign = 'center';
           backgroundText.x = 0;
           backgroundText.y = 130;
           scene.addChild(backgroundText);

           // "くま"を作成
           let kuma = new Sprite(32,32);
           kuma.image = game_.assets['./img/chara1.png'];//くまの画像を設定
           kuma.x = Math.random()*288; //くまの横位置を0〜288pxの間でランダムに表示させる。
           kuma.y = Math.random()*288; //くまの縦位置を0〜288pxの間でランダムに表示させる。
           scene.addChild(kuma);

           let kumaSpeed = Math.random()* 8 - 4;// "くま"のスピードを、-4〜+4の間でランダムに設定
           // "くま"の移動方向が左ならスプライトを反転させる
           if(kumaSpeed>0){
               kuma.scaleX = 1; // (通常)
           }else{
               kuma.scaleX = -1; //(反転)
           }
           //シーンに毎フレームイベントを設定
           scene.addEventListener(Event.ENTER_FRAME, function(){
               time --; //残り時間を1ずつ減らす
               timeLimit.text = '残り時間:' + time; //残り時間の表示を更新

               //時間切れ
               if(time <= 0){
                   game_.replaceScene(createGameoverScene(score));
                   //時間切れになったら、現在表示しているシーンをゲームオーバーシーンに切り替える。
               }
               kuma.x += kumaSpeed; // "くま"を横方向に移動

               // "くま"が画面端に来たときの対処
               if(kuma.x > 320){ //右端に到達したら、
                   kuma.x = -32; //左側にワープさせる
               }else if(kuma.x < -32){ //もしも左側に到達したら、
                   kuma.x = 320; //右側にワープさせる
               }
               // "くま"のスプライトシートのフレームを動かし、走っているような効果を与える
                 // "くま"のフレームを動かす
               if(kuma.frame > 2){ // "くま"のフレームが3以上になったら、
                   kuma.frame = 0; //0に戻す
               }
           });
           // "くま"にタッチイベントを設定
           kuma.addEventListener(Event.TOUCH_START, function(e){
               score++; //"くま"を叩いたらスコア+1
               label.text='スコア:'+score+'体叩いた！'
               kuma.x=Math.random()*288;
               kuma.y=Math.random()*288;
               kumaSpeed=Math.random() *8 - 4;
               if(kumaSpeed>0){
                   kuma.scaleX = 1;
                   kuma.scaleX =-1;
               }
           });
           return scene;
        };

        /*
         *ゲームオーバーシーン 
        */
       let createGameoverScene = function(resultScore){
           let scene = new Scene(); //新たなシーンを作成
           scene.backgroundColor = '#303030';
           //ゲームオーバー画像設定
           let gameOverImage=new Sprite(189,97);
           gameOverImage.image=game_.assets['./img/gameover.png'];
           gameOverImage.x=65;
           gameOverImage.y=112;
           scene.addChild(gameOverImage);//シーンに追加

           //スコアラベル設定
           let label = new Label(resultScore+'体叩いた！');
           label.textAlign='center';
           label.color='#fff';
           label.x=0;
           label.y=60;
           label.font='40px sans-serif';
           scene.addChild(label);

           //リトライラベル設定
           let retryLabel = new Label('もう一度遊ぶ');
           retryLabel.color='#fff';
           retryLabel.x=0;
           retryLabel.y=300;
           retryLabel.font='20px sans-serif';
           scene.addChild(retryLabel);

           //リトライラベルにタッチイベントを設定
           retryLabel.addEventListener(Event.TOUCH_START, function(e){
               game_.replaceScene(createStartScene());//現在表示しているシーンをスタートシーンに置き換える。
           });
           return scene;
       };
       game_.replaceScene(createStartScene());//ゲームのrootSceneをスタートシーンに置き換える。
    }
    game_.start();
};