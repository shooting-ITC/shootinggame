enchant(); //enchant.jsを使用する時必要
window.onload = function(){
    let game = new Game(320,320);/*ゲーム本体を準備すると同時に、表示される領域の大きさを設定*/
    game.fps = 30;
    game.preload(
        './img/chara1.png'
        ); //pre(前)load(読み込み) ゲームで使う素材をあらかじめ読み込み
    game.onload = function(){
        
        let kuma = new Sprite(32,32); 
        /* kumaというスプライト(操作可能な画像)を準備すると同時に、画像の大きさを設定 */
        kuma.frame = 1;
        kuma.image = game.assets['./img/chara1.png']; //kumaにあらかじめロードしておいた画像を適用 ここ大カッコ

        kuma.x = 100; //kumaの横位置
        kuma.y = 120; //kumaの縦位置
        
        //大きさ変更
        // kuma.scaleX = 1;
        // kuma.scaleY = 1;
        kuma.scale(2,2);
        game.rootScene.addChild(kuma); //ゲームのシーンにkumaを表示
        game.rootScene.backgroundColor = "#7ecef4";

        //---------------シーンに「毎フレーム実行イベント」を追加------------
        let speed = 1;
        game.rootScene.addEventListener(Event.ENTER_FRAME, function(){ 
            kuma.x += speed; //毎フレームkumaの座標を右に1pxずつずらす
            
            //回転角度
            // kuma.rotation = 90; //角度の変更
            kuma.rotate(3); //相対的に回転を行う
        });
        //---------------シーンに「タッチイベント」を追加-------------------
        game.rootScene.addEventListener(Event.TOUCH_START, function(e){ 
            //タッチイベントは、タッチした座標をe.x e.y として取ることができます。
            if(e.x > kuma.x){ //もしもタッチした横位置が、kumaの横位置よりも、右側(座標の値として大きい)だったら、
                speed = 1;
            }else{
                speed = -1;
            }
        });
        
       
    }
    game.start(); //ゲームをスタートさせる
};

/*ナレッジ
・jsは誤字脱字を探すのが大変である。


*/