enchant();
window.onload = function(){
    let game_ = new Game(320,320);
    game_.fps = 30;
    game_.onload = function(){

        /*-----タイトルシーンを作り、返す関数-----*/
        let createTitleScene = function(){
            let scene = new Scene(); //新しいシーンの作成
            let label = new Label('タイトルシーン タッチでゲームシーンへ'); //新しいラベル(文字)を作成

            scene.addChild(label); //シーンにラベルを追加
            scene.backgroundColor = 'rgba(255,230,0,1)'; //シーンの背景色を設定
            scene.addEventListener(Event.TOUCH_START, function(e){ //シーンにタッチイベントを設定
                game_.replaceScene(createGameScene()); //現在表示しているシーンをゲームシーンに置き換える。
            });
            return scene; //この関数内で作ったシーンを呼び出し元に返す。
        };


        /*-----ゲームシーンを作り、返す関数-----*/
        let createGameScene = function(){
            let scene = new Scene(); //新しいシーン
            let label = new Label('ゲームシーン タッチでゲームオーバーシーンを重ねる'); //新しいラベル(文字)

            scene.addChild(label);
            scene.backgroundColor = 'rgba(255,200,0,1)';
            scene.addEventListener(Event.TOUCH_START,function(e){
                game_.pushScene(createGameoverScene());
            });
            return scene;
        };

        /*-----ゲームオーバーシーンを作り、返す関数-----*/
        let createGameoverScene = function(){
            let scene = new Scene();
            let label = new Label('ゲームオーバーシーン タッチでゲームシーンに戻る');

            label.x = 0;
            label.y = 20;

            scene.addChild(label);
            scene.backgroundColor = 'rgba(0,0,255,0.5)';
            scene.addEventListener(Event.TOUCH_START,function(e){
                game_.popScene(); //現在表示しているシーンを外し、直前のシーンを表示。
            });
            return scene;
        };
        game_.replaceScene(createTitleScene());
    }
    game_.start();
};